package com.flycms.modules.user.dao;

import com.flycms.modules.user.entity.AdminRoleMenuMerge;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 角色与菜单关联表 数据层
 * 
 * @author 孙开飞
 */
@Repository
public interface AdminRoleMenuMergeDao
{
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    /**
     * 批量新增角色菜单信息
     *
     * @param roleMenuList 角色菜单列表
     * @return 结果
     */
    public int batchRoleMenu(List<AdminRoleMenuMerge> roleMenuList);


    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 通过角色ID删除角色和菜单关联
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleMenuByRoleId(Long roleId);
    
    /**
     * 批量删除角色菜单关联信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoleMenu(Long[] ids);

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 查询菜单使用数量
     * 
     * @param menuId 菜单ID
     * @return 结果
     */
    public int selectCountRoleMenuByMenuId(Long menuId);

    /**
     * 查询角色id和菜单id是否已关联
     *
     * @param roleId
     *        角色id
     * @param menuId
     *        菜单id
     * @return
     */
    public int checkRoleMenuMerge(Long roleId,Long menuId);

    /**
     * 根据角色ID查询所有关联的菜单ID
     *
     * @param roleId 角色ID
     * @return 菜单列表
     */
    public List<Long> selectRoleMenuMerge(Long roleId);
}
