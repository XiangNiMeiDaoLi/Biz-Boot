package com.flycms.modules.user.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.user.entity.UserRole;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 角色表 数据层
 * 
 * @author 孙开飞
 */
@Repository
public interface UserRoleDao extends BaseDao<UserRole>
{
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////




    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    /**
     * 通过角色ID删除角色
     * 
     * @param id 角色ID
     * @return 结果
     */
    public int deleteRoleById(Long id);

    /**
     * 批量角色用户信息
     * 
     * @param id 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoleByIds(Long[] id);

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////

    /**
     * 修改角色信息
     * 
     * @param userRole 角色信息
     * @return 结果....................................................
     */
    public int updateUserRole(UserRole userRole);

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 校验角色名称是否唯一
     * 
     * @param roleName 角色名称
     * @param id 需要排除id
     * @return 角色信息
     */
    public UserRole checkRoleNameUnique(@Param("roleName") String roleName,@Param("id") Long id);
    
    /**
     * 校验角色权限是否唯一
     * 
     * @param roleKey 角色权限
     * @param id 需要排除id
     * @return 角色信息
     */
    public UserRole checkRoleKeyUnique(@Param("roleKey") String roleKey,@Param("id") Long id);

    /**
     * 通过角色ID查询角色
     *
     * @param id 角色ID
     * @return 角色对象信息
     */
    public UserRole findById(Long id);

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<UserRole> selectRolesByUserId(Long userId);

    /**
     * 根据用户ID查询角色相关所有id
     *
     * @param userId 用户ID
     * @return 角色id列表
     */
    public String selectUserRolesByUserId(Long userId);

}
