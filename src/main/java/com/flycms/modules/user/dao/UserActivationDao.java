package com.flycms.modules.user.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.user.entity.UserActivation;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface UserActivationDao extends BaseDao<UserActivation> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 按用户名（邮箱、手机号）+ 验证码查询修改验证状态为已验证，0未验证，1为已验证
     *
     * @param userName
     *         需要查询的手机号码或者邮箱
     * @param code
     *         验证码
     * @return
     */
    public int updateUserActivationByStatus(@Param("userName") String userName,@Param("code") String code);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询验证码在当前时间5分钟内获取并且是否过时或不存在
     *
     * @param userName
     *         查询的用户名
     * @param codeType
     *         查询的验证码类型，1手机注册验证码,2安全手机设置验证码,3密码重置验证码
     * @return
     */
    public UserActivation findByUserActivationCode(@Param("userName") String userName,@Param("codeType") Integer codeType);

    /**
     * 查询指定日期内申请验证码次数
     *
     * @param userName
     * @param addTime
     * @return
     */
    public int checkUserActivationCount(@Param("userName") String userName,@Param("addTime") String addTime);

}
