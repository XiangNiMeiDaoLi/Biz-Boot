package com.flycms.modules.user.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 登录用户临时存储实体类
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 2:15 2019/8/28
 */
@Setter
@Getter
public class LoginUser  implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String userName;
    private String password;
    private int loginType;
}
