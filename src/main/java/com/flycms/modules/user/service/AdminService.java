package com.flycms.modules.user.service;

import com.flycms.common.utils.result.Result;
import com.flycms.modules.user.entity.Admin;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 11:19 2019/8/18
 */
public interface AdminService {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    public Result addAdmin(Admin admin);
    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    public Result updatePassword(String oldPassword, String password);


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    //通过id查询用户信息
    public Admin findById(Long userId);

    //通过username查询用户信息
    public Admin findByUsername(String userName);

    /**
     * 翻页列表
     *
     * @param name
     *         根据分类名查询
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    public Object selectUserListPager(String name, Integer page, Integer limit, String sort, String order);
}
