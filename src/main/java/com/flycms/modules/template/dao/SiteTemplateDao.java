package com.flycms.modules.template.dao;

import com.flycms.common.dao.BaseDao;
import com.flycms.modules.template.entity.SiteTemplateMerge;
import com.flycms.modules.template.entity.SiteTemplatePage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 10:08 2019/6/9
 */
@Repository
public interface SiteTemplateDao extends BaseDao<SiteTemplatePage> {
    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////

    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////


    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////

    /**
     * 查询网站id和类别查询模板名称是否重复
     *
     * @param siteId
     *         网站id
     * @param moduleId
     *         模型fly_system_module表id；对应的如新闻模块id，产品模块id
     * @param templateName
     *         用户自定义模板名称
     * @param pageType
     *         模板类型，0网站首页，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板
     * @return
     */
    public int checkSiteTPByName(@Param("siteId") Long siteId,
                                 @Param("moduleId") Long moduleId,
                                 @Param("templateName") String templateName,
                                 @Param("pageType") Integer pageType
    );

    /**
     * 按siteId当前网站使用模板信息
     *
     * @param siteId
     *         网站id
     * @return
     */
    public SiteTemplateMerge findSiteTemplateBySiteId(@Param("siteId") Long siteId);
}
