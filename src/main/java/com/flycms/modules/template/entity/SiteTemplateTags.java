package com.flycms.modules.template.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: ${Description}
 * @email 79678111@qq.com
 * @Date: 20:34 2019/9/30
 */
@Setter
@Getter
public class SiteTemplateTags implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;			    //编号
    private Long siteId;		    //所属网站的id
    private Long templateId;		//所属模板id
    private String tagKey;        //针对模板调用的key
    private String tagValue;      //标签内容
    private LocalDateTime addTime;         //添加时间
}
