package com.flycms.modules.template.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 模版页面
 * @author 孙开飞
 */
@Setter
@Getter
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SiteTemplatePageVO implements Serializable {
	@JsonSerialize(using = ToStringSerializer.class)
	private Long id;			    //编号
	private Integer pageType;        //模板类型，0网站首页，1频道封面（频道首页）模板，2列表模板，3内容模板，4单独页面模板
	private String templateName; //所属的模版名字
}