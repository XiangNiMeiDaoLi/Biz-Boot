package com.flycms.modules.notify.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class SmsSign implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    //接口id
    private String apiId;
    //签名名称
    private String signName;
    //签名描述
    private String remark;
    private String createTime;
    private String status;
}
