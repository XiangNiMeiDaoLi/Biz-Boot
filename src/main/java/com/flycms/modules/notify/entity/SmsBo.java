package com.flycms.modules.notify.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 短信业务对象实体类
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @Description: 主要提供短信接口查询集合查询
 * @email 79678111@qq.com
 * @Date: 11:53 2019/9/2
 */
@Setter
@Getter
public class SmsBo implements Serializable {
    private static final long serialVersionUID = 1L;
    //指定用户ID
    private Long userId;
    //接口密钥id
    private String accessKeyId;
    //接口密钥
    private String accessKeySecret;
    //签名名称
    private String signName;
    //模板编码
    private String  templateCode;
}
