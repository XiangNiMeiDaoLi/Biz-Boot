package com.flycms.modules.finance.controller;

import com.flycms.common.controller.BaseController;
import com.flycms.modules.finance.service.OrderService;
import com.flycms.modules.help.service.HelpService;
import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 帮助说明
 * @author 孙开飞
 */
@Controller
@RequestMapping("/member/order/")
public class OrderMemberController extends BaseController {
    @Autowired
    private OrderService orderService;

	/**
	 * 订单列表
	 */
	@RequestMapping("/list{url.suffix}")
	public String helpList(String siteid, String title,
                           @RequestParam(defaultValue = "1") Integer page,
                           @RequestParam(defaultValue = "10") Integer limit,
                           @RequestParam(defaultValue = "add_time") String sort,
                           @RequestParam(defaultValue = "desc") String order,Model model){
        Object pager=orderService.queryOrderPager(NumberUtils.toLong(siteid),title,page,limit,sort,order);
        model.addAttribute("siteid", siteid);
        model.addAttribute("title", title);
        model.addAttribute("pager",pager);
		return "member/order/list";
	}

	
	
}
