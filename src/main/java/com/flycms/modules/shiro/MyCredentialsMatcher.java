package com.flycms.modules.shiro;

import com.flycms.common.utils.bcrypt.BCryptPasswordEncoder;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.CredentialsMatcher;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 *
 * @author 孙开飞
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:18 2019/8/17
 */
public class MyCredentialsMatcher implements CredentialsMatcher {

  @Override
  public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
    String rawPassword = String.valueOf((char[]) token.getCredentials());
    String encodedPassword = String.valueOf(info.getCredentials());
    return new BCryptPasswordEncoder().matches(rawPassword, encodedPassword);
  }
}
