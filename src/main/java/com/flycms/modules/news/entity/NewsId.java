package com.flycms.modules.news.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 */
@Setter
@Getter
public class NewsId implements Serializable {
	private static final long serialVersionUID = 1L;

	// Fields

	private Integer id;
	private Integer supportnum;
	private Integer sharenum;
}