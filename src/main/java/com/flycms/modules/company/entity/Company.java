package com.flycms.modules.company.entity;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 23:04 2019/8/17
 */
@Setter
@Getter
public class Company implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String shortName;
    private String fullName;
    private String companyType;
    private Long industry;
    private String mode;
    private String foundTime;
    private String capital;
    private String regunit;
    private String business;
    private Long scale;
    private String sell;
    private String buy;
    private String introduce;
    private Long contactId;
    private Integer sortOrder;
    private LocalDateTime createTime;
    private LocalDateTime updateTime;
    private Integer verified;
    private Integer recommend;
    private Integer deleted;
}
