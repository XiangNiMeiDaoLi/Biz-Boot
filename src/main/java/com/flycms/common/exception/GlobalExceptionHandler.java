package com.flycms.common.exception;

import com.flycms.common.utils.result.LayResult;
import com.flycms.common.utils.result.R;
import com.flycms.common.utils.result.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 自定义异常
 *
 * @author 孙开飞
 */
@ControllerAdvice
public class GlobalExceptionHandler {

  private Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  private HttpStatus getStatus(HttpServletRequest request) {
    Integer statusCode = (Integer) request.getAttribute("javax.servlet.error.status_code");
    if (statusCode == null) {
      return HttpStatus.INTERNAL_SERVER_ERROR;
    }
    return HttpStatus.valueOf(statusCode);
  }

  /**
   * 接口错误统一处理
   *
   * @param e
   * @return
   * @throws Exception
   */
  @ExceptionHandler(value = PageException.class)
  public String pageErrorHandler(PageException e,Model model) {
    model.addAttribute("message",e.getMessage());
    return "404";
  }

  /**
   * 接口错误统一处理
   *
   * @param e
   * @return
   * @throws Exception
   */
  @ExceptionHandler(value = ApiException.class)
  @ResponseBody
  public R jsonErrorHandler(ApiException e) {
    return new R(e.getCode(), e.getMessage(),  null);
  }
}
